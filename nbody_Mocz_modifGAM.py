import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

"""
Create Your Own N-body Simulation (With Python)
Philip Mocz (2020) Princeton Univeristy, @PMocz

Simulate orbits of stars interacting due to gravity
Code calculates pairwise forces according to Newton's Law of Gravity

Some modifications by Gary Mamon
"""

def getAcc( pos, mass, G, softening ):
	"""
    Calculate the acceleration on each particle due to Newton's Law 
	pos  is an N x 3 matrix of positions
	mass is an N x 1 vector of masses
	G is Newton's Gravitational constant
	softening is the softening length
	a is N x 3 matrix of accelerations
	"""
	# positions r = [x,y,z] for all particles
	x = pos[:,0:1]
	y = pos[:,1:2]
	z = pos[:,2:3]

	# matrix that stores all pairwise particle separations: r_j - r_i
	dx = x.T - x
	dy = y.T - y
	dz = z.T - z

	# matrix that stores 1/r^3 for all particle pairwise particle separations 
# 	inv_r3 = (dx**2 + dy**2 + dz**2 + softening**2)
	inv_r3 = (dx*dx + dy*dy + dz*dz + softening*softening) # G. Mamon
	inv_r3[inv_r3>0] = inv_r3[inv_r3>0]**(-1.5)

	ax = G * (dx * inv_r3) @ mass
	ay = G * (dy * inv_r3) @ mass
	az = G * (dz * inv_r3) @ mass
	
	# pack together the acceleration components
	a = np.hstack((ax,ay,az))

	return a
	
def getEnergy( pos, vel, mass, G, soft=0 ):
	"""
	Get kinetic energy (KE) and potential energy (PE) of simulation
	pos is N x 3 matrix of positions
	vel is N x 3 matrix of velocities
	mass is an N x 1 vector of masses
	G is Newton's Gravitational constant
	KE is the kinetic energy of the system
	PE is the potential energy of the system
	"""
	# Kinetic Energy:
	KE = 0.5 * np.sum(np.sum( mass * vel**2 ))


	# Potential Energy:

	# positions r = [x,y,z] for all particles
	x = pos[:,0:1]
	y = pos[:,1:2]
	z = pos[:,2:3]

	# matrix that stores all pairwise particle separations: r_j - r_i
	dx = x.T - x
	dy = y.T - y
	dz = z.T - z

	# matrix that stores 1/r for all particle pairwise particle separations 
# 	inv_r = np.sqrt(dx**2 + dy**2 + dz**2)
	inv_r = np.sqrt(dx*dx + dy*dy + dz*dz + soft*soft) # G. Mamon
	inv_r[inv_r>0] = 1.0/inv_r[inv_r>0]

	# sum over upper triangle, to count each interaction only once
	PE = G * np.sum(np.sum(np.triu(-(mass*mass.T)*inv_r,1)))
	
	return KE, PE;


def do_run(N=20,tEnd=100,dt=0.01,soft=0.05,Esoft=0,
            model='Plummer',scaleRadius=1,Mtot=1e11,
            vFactor=1,MextraFrac=0,rExtra=None,
            radiusUnit='kpc',velocityUnit='km/s',massUnit='solar',
            timeUnit='Gyr',
            trails=True,plotRealTime=False,seed=1,savefig=None,verbose=0):
    """ 
    run N-body simulation with leap-frog particle advancement
    using Philip Mocsz' code
    for equal mass particles plus an optional heavy particle
    
    the user is free to chose units for sizes, velocities and masses
     the code automatically computes Newton's gravitational constant 
         in these units
    
    arguments:
        N: number of particles (default 20)
        tEnd: end time of simulation (in units of timeUnit)
        dt: timestep of simulation (in units of timeUnit)
        soft: gravitational softening for close encounters (in units of radiusUnit)
        Esoft: gravitational softening for estimating potential energy (same units)
        model: system model ('Plummer' or 'Hernquist')
        scaleRadius: scaleRadius of model (un units of radiusUnit)
        Mtot: total mass of model (in units of massUnit)
        vFactor: multiplicative dimensionless factor for velocities 
            (e.g. << 1 for collapse, default 1)
        MextraFrac: fraction of extra mass in circular orbit (default 0)
        rExtra: radius of orbit of extra mass (in units of scale radius)
        radiusUnit: unit of radii (string, e.g. 'AU', 'pc', 'kpc')
        velocityUnit: unit of velocities (string, e.g. 'km/s', '100 km/s')
        massUnit: unit of masses (string, e.g. 'solar', '10^11 solar')
        timeUnit: unit of times for tEnd, dt, and plotting 
            (string, e.g. 'year','Myr','Gyr')
        trails: follow trails of trajectories for real-time plot (bool, default True)
        plotRealTime: plot in real time (bool, default False, 
                                         avoid True unless you have previously entered
                                         %matplotlib qt. Code will be much slower)
        seed: seed for random number generator
            (int, to reproduce results, or else None for truly random)
        savefig: figure prefix (default None for no figure saved)
        verbose: level of verbosity (0 for quiet, default 0)
        
    returns (for other figures):
        times (1D array, in chosen time units)
        positions (N_particles,3,N_times) array in chosen radius units
        velocities (N_particles,3,N_times) array in chosen velocity units
        kinetic energies (N_particles,N_times) array in square of chosen velocity units
        potential energies (N_particles,N_times) array
        radial coordinates (N_particles,N_times) array
        radial velocities (N_particles,N_times) array

    author: Gary Mamon, very heavily based on main program of Philip Mocz
    """

    # number of particles
    N_particles = N
    
    # Units
    G,tUnit_s,tUnit_Plot,time_label = get_G_tUnit(radiusUnit=radiusUnit,
                                                 velocityUnit=velocityUnit,
                                                 massUnit=massUnit,
                                                 timeUnit=timeUnit)
    
	# Simulation parameters
    t = 0
    pos, vel, mass = InitialConditions(N_equal=N_particles,model=model,
                                       scaleRadius=scaleRadius,soft=soft,
                                       M_total=Mtot,
                                       vFactor=vFactor,MextraFrac=MextraFrac,
                                       rExtra=rExtra,
                                       G=G,seed=seed,verbose=verbose)
    r = np.sqrt(np.sum(pos*pos,axis=1))
    v_r = np.sum(pos*vel,axis=1) / r
    r_median = np.median(r)
    x_lims_4plot = [-1.5*r_median,1.5*r_median]
    y_lims_4plot = [-1.5*r_median,1.5*r_median]
	
    # initial gravitational accelerations
    acc = getAcc( pos, mass, G, soft )
    
    # initial energy of system
    KE, PE  = getEnergy( pos, vel, mass, G, Esoft )
    
    # number of timesteps
    N_times = int(np.ceil(tEnd/dt))
    
    # save energies, particle orbits for plotting trails
    if MextraFrac > 0:
        N_particles += 1
    pos_save = np.zeros((N_particles,3,N_times+1))
    pos_save[:,:,0] = pos
    vel_save = np.zeros((N_particles,3,N_times+1))
    vel_save[:,:,0] = vel
    r_save = np.zeros((N_particles,N_times+1))
    r_save[:,0] = r
    vr_save = np.zeros((N_particles,N_times+1))
    vr_save[:,0] = v_r
    KE_save = np.zeros(N_times+1)
    KE_save[0] = KE
    PE_save = np.zeros(N_times+1)
    PE_save[0] = PE
    t_all = np.arange(N_times+1)*dt
    
    # prepare figure
    if plotRealTime:
        # fig = plt.figure(figsize=(4,5), dpi=80)
        # grid = plt.GridSpec(3, 1, wspace=0.0, hspace=0.3)
        # ax1 = plt.subplot(grid[0:2,0])
        # ax2 = plt.subplot(grid[2,0])
        # ax1 = plt.subplots()
        plt.xlabel('x (' + radiusUnit + ')')
        plt.ylabel('y (' + radiusUnit + ')')
        
        colors_all = colors * (1+int(N/len(colors)))
        colors_all = colors_all[0:N]
    
    if plotRealTime:
        ans = input("For plotting in real time, did you first type '%matplotlib qt'? (y/n): ")
        if ans != 'y':
            sys.exit(0)
    
    # loop over timesteps
    for i in range(N_times):
        # (1/2) kick
        vel += acc * dt/2.0
        
        # drift
        pos += vel * dt
        
        # center of mass
        # print("shapes pos mass =",pos.shape,mass.shape)
        pos_cen = np.average(pos,weights=mass[:,0],axis=0)
        dpos = pos - pos_cen
        
        # radial quaN_timesities
        r = np.sqrt(np.sum(dpos*dpos,axis=1))
        v_r = np.sum(pos*vel,axis=1) / r
        
        # update accelerations
        acc = getAcc( pos, mass, G, soft )
        
        # (1/2) kick
        vel += acc * dt/2.0
        
        # update time
        t += dt
        
        # get energy of system
        KE, PE  = getEnergy( pos, vel, mass, G, Esoft )
        
        # save energies, positions for plotting trail
        pos_save[:,:,i+1] = pos
        vel_save[:,:,i+1] = vel
        KE_save[i+1] = KE
        PE_save[i+1] = PE
        r_save[:,i+1] = r
        vr_save[:,i+1] = v_r
        
        # plot in real time
        # if plotRealTime or (i == N_times-1):
        if plotRealTime:
            # plt.sca(ax1)
            # plt.cla()
            xx = pos_save[:,0,max(i-50,0):i+1]
            yy = pos_save[:,1,max(i-50,0):i+1]
            if verbose >= 2:
                print("before two scatter plots for i t=",i,t)
            plt.scatter(xx,yy,s=1,color=[.7,.7,1])
            # priN_times("i len xx  colors_all = ",i,len(xx),len(colors_all))
            # plt.scatter(xx,yy,s=1,color=colors_all)
            if plotRealTime:
                plt.scatter(pos[:,0],pos[:,1],s=10,color='blue')
            # plt.scatter(pos[:,0],pos[:,1],s=10,color='blue')

            plt.xlim(x_lims_4plot)
            plt.ylim(y_lims_4plot)
            if verbose >= 2:
                print("before set_aspect for i t=",i,t)
            plt.gca().set_aspect('equal', 'box')
            # ax1.set_xticks([-2,-1,0,1,2])
            # ax1.set_yticks([-2,-1,0,1,2])
            
            # plt.sca(ax2)
            # plt.cla()
            # plt.scatter(t_all,KE_save,color='red',s=1,label='KE' if i == N_times-1 else "")
            # plt.scatter(t_all,PE_save,color='blue',s=1,label='PE' if i == N_times-1 else "")
            # plt.scatter(t_all,KE_save+PE_save,color='black',s=1,label='Etot' if i == N_times-1 else "")
            # ax2.set(xlim=(0, tEnd), ylim=(-300, 300))
            # ax2.set_aspect(0.07)
            
            if verbose >= 2:
                print("before plt.pause for i t=",i,t)
            plt.pause(0.001)
            if verbose >= 2:
                print("after plt.pause for i t=",i,t)
    # add labels/legend
    # plt.sca(ax2)
    # plt.xlabel('time')
    # ax2.set_xlabel('time (' + time_label + ')' )
    # ax2.set_ylabel('energy')
    # ax2.legend(loc='upper right')
    
    # Save figure
    # plt.savefig('nbody.png',dpi=240)
    if verbose >= 2:
        print("now out of time loop")
    if (savefig is not None) & plotRealTime:
        plt.savefig(savefig + '.pdf')
    # plt.show()
        
    return t_all, pos_save, vel_save, KE_save, PE_save, r_save, vr_save
    
# additions by G. Mamon

import sys
# import os

# constants in CGS units
G_cgs = 6.6743e-8
M_Sun_cgs = 1.9885e33
pc_cgs = 3.0857e18
kpc_cgs = 1000 * pc_cgs
AU_cgs = 1.49598e13
year_cgs = 365.2425 * 86400
Myr_cgs = 1e6 * year_cgs
Gyr_cgs = 1e9 * year_cgs
prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

def InitialConditions(N_equal,model='Plummer',scaleRadius=1,soft=0.01,
                      M_total=None,
                      G=None,vFactor=1,MextraFrac=0,rExtra=1,
                      seed=1,verbose=0):
    """ generate initial positions and velocities
    
    arguments:
        N_equal: number of equal-mass particles
        model: density model ('Plummer' or 'Hernquist', default: 'Plummer')
        scaleRadius: scale radius of model in radiusUnit units (default 1)
        soft: force softening in radiusUnit units
            force modulus = sum_j≠i G m_j / (sep^2_+soft^2)
        M_total: total mass (default None)
        G: Newton's gravitationbal constant (default None, but computed my do_run)
        vFactor: multiplicative factor on initial velocities (default 1): 
            use vFactor close to 0 to simulate a global collapse
        MextraFrac: mass of extra particle (units of total mass of equal-mass particles,
                                            default 0)
        rExtra: position of extra particle (unit of scale radius, default 1)
        seed: random seed (integer, default 1)
        verbose: verbosity (default 0 for quiet)
        
    returns:
        pos, vel, mass
        pos: positions ([N_particles,3] array)
        vel: velocities ([N_particles,3] array)
        mass: masses ([Nparticles,1] array) (not [N_particles] for practical reasons)
        
    author: Gary Mamon
    """

    # Generate Initial Conditions
    rng = np.random.default_rng(seed)
    
    # uniform [0-1] random arrays for r, theta and phi
    q_r = rng.random(N_equal)
    q_theta = rng.random(N_equal)
    q_phi = rng.random(N_equal)
    
    # spherical coordinates:
    # r in physical units
    if model == "Plummer":
        r = scaleRadius * q_r**(1/3)/np.sqrt(1-q_r**(2/3))
    elif model == "Hernquist":
        r = scaleRadius * np.sqrt(q_r)/(1-np.sqrt(q_r))
    else:
        raise ValueError("Cannot recognize model = " + model)

    # angular spherical coordinates (radians)
    theta = np.arccos(1-2*q_theta)
    phi = 2*np.pi*q_phi

    # cartesian positions in working (rescaled) units
    x = r*np.sin(theta)*np.cos(phi)
    y = r*np.sin(theta)*np.sin(phi)
    z = r*np.cos(theta)
    
    # dimensionless radii
    u = r/scaleRadius
    
    # isotropic velocity dispersion in working units
    sigma = np.sqrt(G*M_total/scaleRadius) * sigmaiso_tilde(u,model)
    if verbose > 0:
        print("G = ", G)
        print("mean sigma=",sigma.mean())

    # cartesian velocities (isotropic orbits) in 100 km/s
    vx = vFactor * rng.normal(0,sigma,N_equal)
    vy = vFactor * rng.normal(0,sigma,N_equal)
    vz = vFactor * rng.normal(0,sigma,N_equal)
    
    # subtract bulk motion (equal mass particles)
    if N_equal > 1:
        vx = vx - np.mean(vx)
        vy = vy - np.mean(vy)
        vz = vz - np.mean(vz)
    
    # masses 
    m = M_total/N_equal*np.ones(N_equal).reshape(N_equal,1)
    
    # extra mass
    if MextraFrac > 0:
        x = np.append(x,rExtra*scaleRadius)
        y = np.append(y,0)
        z = np.append(z,0)
        vx = np.append(vx,0)
        vy = np.append(vy,np.sqrt(G*MextraFrac*M_total
                                  *M_tilde(rExtra,model)))
        # rExtra is in units of scaleRadius, hence dimensionless
        vz = np.append(vz,0)
        mExtra = np.array([M_total*MextraFrac]).reshape(1,1)
        if verbose > 0:
            print("mExtra=",mExtra)
        m = np.concatenate([m,mExtra])
    
    if verbose > 0:
        print("sum m=",np.sum(m))
    pos = np.array([x,y,z]).T
    vel = np.array([vx,vy,vz]).T
    return pos, vel, m
    
def get_G_tUnit(radiusUnit='kpc',velocityUnit='km/s',massUnit='solar',
                timeUnit='Myr'):
    """ gravitational constant and timeUnit for given radius, velocity and mass units
    arguments:
        radiusUnit: unit of sizes, radii ('AU','pc','kpc', default: 'kpc')
        velocityUnit: unit of velocities ('km/s', '100 km/s', default: 'km/s')
        massUnit: unit of masses ('solar', '10^11 solar', default: 'solar')
        
    returns:
        GNewton: gavitational constant in desired units
        timeUnit_new_s: new timeUnit in seconds
        timeUnit_new: new timeUnit in units of old time unit
        timeLabel: time label for plots
        
    author: Gary Mamon
    """

    # compute Newton's constant and internal time unit
    if massUnit == 'solar':
        massUnit_g = M_Sun_cgs
    elif massUnit == '10^11 solar':
        massUnit_g = 1e11 * M_Sun_cgs
    else:
        massUnit_g = 1
    if radiusUnit == 'pc':
        radiusUnit_cm = pc_cgs
    elif radiusUnit == 'kpc':
        radiusUnit_cm = 1000*pc_cgs
    elif radiusUnit == 'AU':
        radiusUnit_cm = AU_cgs
    else:
        radiusUnit_cm = 1
    if velocityUnit == 'km/s':
        velocityUnit_cms = 1e5
    elif velocityUnit == '100 km/s':
        velocityUnit_cms = 1e7
    else:
        velocityUnit_cms = 1
    timeLabel = timeUnit
    if timeUnit == 'year':
        timeUnit_s = year_cgs
    elif timeUnit == 'Myr':
        timeUnit_s = Myr_cgs
    elif timeUnit == 'Gyr':
        timeUnit_s = Gyr_cgs
    else:
        timeUnit_s = 1
        timeLabel = 'seconds'
        
    # gravitational constant
    GNewton = G_cgs * massUnit_g / (radiusUnit_cm * velocityUnit_cms**2)
    
    # timeUnit in seconds
    timeUnit_new_s = radiusUnit_cm/velocityUnit_cms
    
    # timeUnit in old timeUnit
    timeUnit_new = timeUnit_new_s / timeUnit_s
    
    return GNewton, timeUnit_new_s, timeUnit_new, timeLabel


def plot_trajectories(t,p,axes='xy',xylim=None,savefig=None,MextraFrac=0,
              rUnit=None,lw=2):
    """ plot trajectories of particles
    arguments:
        t: array of times
        p: array of positions (N_particles, 3, N_timesteps)
        axes: choice of two axes ('xy','xz',or 'yz') [default 'xy']
        lw: line weight (default 2)
        xylims: absolute (single) limit of trajectory plot box (default None)
        mExtraFrac: mass fraction of extra particle (default 0)
        rUnit: unit for labels (default '')
        
    author: Gary Mamon
    """
    print("shape p = ",p.shape)
    coordinates = 'xyz'
    axis_indices = []
    for j, axis in enumerate(axes):
        ii = coordinates.index(axis)
        axis_indices.append(ii)
    print("axis_indices=",axis_indices)
    # for k in range(len(t)):
        
    # number of equal-mass particles
    N_particles = len(p)
    if MextraFrac > 0:
        N_equal = N_particles-1
    else:
        N_equal = N_particles
    for i in range(N_equal):
        plt.plot(p[i,axis_indices[0],:],p[i,axis_indices[1],:],lw=lw)
    if MextraFrac > 0:
        plt.plot(p[N_particles-1,axis_indices[0],:],
                 p[N_particles-1,axis_indices[1],:],
                 lw=3,color='k')
    # last timestep
    plt.scatter(p[:,axis_indices[0],-1],p[:,axis_indices[1],-1])
    if MextraFrac > 0:
        plt.scatter([p[N_particles-1,axis_indices[0],-1]],
                    [p[N_particles-1,axis_indices[1],-1]],s=100,c='k')
    x_label = '$' + axes[0] + '$'
    y_label = '$' + axes[1] + '$'
    if rUnit is not None:
        x_label += ' (' + rUnit + ')'
        y_label += ' (' + rUnit + ')'
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    if xylim is not None:
        plt.xlim(-xylim,xylim)
        plt.ylim(-xylim,xylim)
    if savefig is not None:
        plt.savefig(savefig.replace('.pdf','') + '.pdf')
    else:
        plt.show()
              
def plot_roft(t,r,connect=True,tlims=None,rlims=None,yLog=False,savefig=None,
              MextraFrac=0,rUnit=None,timeLabel=None,lw=2):
    """ plot radial distances vs. time of particles
    arguments:
        t: array of times
        r: array of radial coordinates positions (N_particles, N_timesteps)
        lw: line weight (default 2)
        xylims: absolute (single) limit of trajectory plot box (default None)
        rUnit: unit for labels (default '')
        
    author: Gary Mamon
    """
    N = len(r)
    plt.figure(figsize=(6,6))
    if yLog:
        plt.yscale('log')
    if tlims is not None:
        plt.xlim(tlims)
    if rlims is not None:
        plt.ylim(rlims)
    
    # number of equal-mass particles
    if MextraFrac > 0:
        Nlight = N-1
    else:
        Nlight = N
    for k in range(Nlight):
        if connect:
            plt.plot(t,r[k,:],lw=lw)
        else:
            plt.scatter(t,r[k,:])

    # geometric mean
    if connect:
        rGeomean = stats.mstats.gmean(r[0:Nlight,:],axis=0)
        rMedian = np.median(r[0:Nlight,:],axis=0)
        plt.plot(t,rGeomean,color='b',lw=3,label='Geometric mean')
        plt.plot(t,rMedian,color='b',lw=3,ls='--',label='Median')
        
    # add massive particle in thick black
    if N > Nlight:
        rMassive = r[N-1,:]
        if connect:
            plt.plot(t,rMassive,color='k',lw=3,label='Massive particle')
        else:
            plt.scatter(t,rMassive,c='k')
    else:
        print("N = ", N)
    x_label = '$t$'
    y_label = '$r$'
    if timeLabel is not None:
        x_label += ' (' + timeLabel + ')'
    if rUnit is not None:
        y_label += ' (' + rUnit + ')'
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='best')
    if savefig is not None:
        plt.savefig(savefig.replace('.pdf','') + '.pdf')
    else:
        plt.show()
    
def plot_Eoft(t,KE,PE,connect=True,tlims=None,rlims=None,savefig=None,
              lw=2,timeLabel=None,velocityUnit=None,massUnit=None):
    """ plot kinetic, potential and total energies of system vs. time
    arguments:
        t: array of times
        KE, PE: arrays of kinetic and potential energies (length N_timesteps)
        lw: line weight (default 2)
        xylims: absolute (single) limit of trajectory plot box (default None)
        velocityUnit: unit for velocities
        massUnit: unit for masses (e.g. 'solar')
        
    author: Gary Mamon
    """
    E_tot = KE+PE
    if connect:
        plt.plot(t,KE,'r-',label='kinetic',lw=lw)
        plt.plot(t,PE,'b-',label='potential',lw=lw)
        plt.plot(t,E_tot,'k-',label='energy',lw=lw)
    else:
        plt.scatter(t,KE,c='r',label='kinetic')
        plt.scatter(t,PE,c='b',label='potential')
        plt.scatter(t,E_tot,c='k',label='total')
    plt.axhline(0,lw=1,color='g')
    plt.xlabel('time')
    if tlims is not None:
        plt.xlim(tlims)
    x_label = '$t$'
    y_label = 'energies'
    if timeLabel is not None:
        x_label += ' (' + timeLabel + ')'
    if (velocityUnit is not None) & (massUnit is not None):
        if massUnit == 'solar':
            massUnit = '$\mathrm{M}_\odot$'
        y_label += ' [' + massUnit + '\,(' + velocityUnit + ')${}^2$]'
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend()
    plt.title('Energy is conserved to %.2f'%(100*(E_tot[-1]/E_tot[0]-1)) + '\%')
    if savefig is not None:
        plt.savefig(savefig.replace('.pdf','') + '.pdf')
    else:
        plt.show()
    
def MPlummer_tilde(x):
    """dimensionless mass profile of Plummer (1911) model
    argument: r/a
    returns: M(r)/M_infinity
    author: Gary Mamon"""

    return x*x*x/(1+x*x)**1.5

def MHernquist_tilde(x):
    """dimensionless mass profile of Hernquist (1990) model
    argument: r/a
    returns: M(r)/M_infinity
    author: Gary Mamon"""
    
    return x*x/(1+x)**2

def M_tilde(x,model):
    """dimensionless mass profile
    arguments: r/a, model
    returns: M(r)/M_infinity
    author: Gary Mamon"""

    if model == "Hernquist":
        return MHernquist_tilde(x)
    elif model == "Plummer":
        return MPlummer_tilde(x)
    else:
        raise ValueError("cannot recognize model " + model)

def sigmaisoPlummer_tilde(x):
    """dimensionless isotropic velocity dispersion profile of Plummer model
    argument: r/a
    returns: sigma_iso(r) / sqrt[GM_inf/a]
    author: Gary Mamon"""

    sigma2 = 1/6 / np.sqrt(x*x+1)
    return np.sqrt(sigma2)

def sigmaisoHernquist_tilde(x):
    """dimensionless isotropic velocity dispersion profile of Hernquist model
    argument: r/a
    returns: sigma_iso(r) / sqrt[GM_inf/a]
    author: Gary Mamon"""

    x1 = x+1
    # need series expansion to avoid numerical errors at large x
    sigma2 = np.where(x>1000,0.2/x-7/30/(x*x),x*x1**3*np.log(x1/x) - x*(25+2*x*(26+3*x*(7+2*x)))/(12*x1))
    return np.sqrt(sigma2)

def sigmaiso_tilde(x,model):
    """dimensionless isotropic velocity dispersion profile
    arguments: r/a, model
    returns: sigma_iso(r)/sqrt[GM_inf/a]
    author: Gary Mamon"""

    if model == "Hernquist":
        return sigmaisoHernquist_tilde(x)
    elif model == "Plummer":
        return sigmaisoPlummer_tilde(x)
    else:
        raise ValueError("cannot recognize model " + model)

def Phi_tilde_Hernquist(x):
    """dimensionless gravitational potential of Hernquist model
    argument: r/a
    returns: -Phi(r) / G M_tot/a
    """
    return 1/(x+1)

def Phi_tilde_Plummer(x):
    """dimensionless gravitational potential of Plummer model
    argument: r/a
    returns: -Phi(r) / G M_tot/a
    """
    return 1/np.sqrt(x*x+1)

def Phi_tilde(x,model):
    """dimensionless gravitational potential
    arguments: 
        x: r/a
        model: density model
    returns: -Phi(r) / G M_tot/a
    """
    if model == 'Hernquist':
        return Phi_tilde_Hernquist(x)
    if model == 'Plummer':
        return Phi_tilde_Plummer(x)
    else:
        raise ValueError("Cannot recognize model " + model)
        
def rhalf_over_a(model):
    """half mass radius over scale radius for a given model
    argument: model ('Plummer' or 'Hernquist')
    returns: r_half/a
    """
    if model == 'Hernquist':
        return 1+np.sqrt(2)
    elif model == 'Plummer':
        return (1+2**(1/3))/np.sqrt(3)
    else:
        raise ValueError("cannot recognize model=" + model)
        
# if __name__== "__main__":
#   main()
